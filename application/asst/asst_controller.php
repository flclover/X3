<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class asst_controller extends base_controller {
    function get_badge_count() {
        //读取数据库模块列表生成菜单项
        $node_list = model('node') -> access_list();

        $system_folder_menu = model('system_folder') -> get_folder_menu();

        $user_folder_menu = model('user_folder') -> get_folder_menu();

        $node_list = array_merge($node_list, $system_folder_menu, $user_folder_menu);

        $system_folder = model('system_folder') -> get_field('id,controller');

        foreach ($node_list as $val) {
            $badge_function = $val['badge_function'];
            if (!empty($badge_function) and ($badge_function != 'sum')) {
                if ($badge_function == 'system_folder' or $badge_function == 'user_folder') {
                    $model_name = $system_folder[$val['fid']];
                    if (method_exists(model($model_name), 'badge_count')) {
                        $badge_count[$val['id']] = model($model_name) -> badge_count($val['fid']);
                    }
                } else {
                   // echo $badge_function;
                    $tmp = explode('/', $badge_function);
                    if (count($tmp) == 1) {
                        $model_name = $tmp[0];
                        $params = null;
                    }
                    if (count($tmp) == 2) {
                        $model_name = $tmp[0];
                        $params = $tmp[1];
                    }
                    $badge_count[$val['id']] = model($model_name) -> badge_count($params);
                }
            }
        };

        foreach ($node_list as $key => $val) {
            if ($val['badge_function'] == 'sum') {
                $child_menu = list_to_tree($node_list, $val['id']);
                $child_menu = tree_to_list($child_menu);
                //dump($child_menu);
                $child_menu_id = rotate($child_menu);
                $count = 0;
                if (isset($child_menu_id['id'])) {
                    $child_menu_id = $child_menu_id['id'];
                    $count = 0;
                    foreach ($child_menu_id as $k1 => $v1) {
                        if (!empty($badge_count[$v1])) {
                            $count += $badge_count[$v1];
                        }
                    }
                }
                $badge_sum[$val['id']] = $count;
            }
        };

        if (!empty($badge_count)) {
            if (!empty($badge_sum)) {
                $total = $badge_count + $badge_sum;
            } else {
                $total = $badge_count;
            }
            $this -> assign('badge_count', $total);
        }
        ajax_return($total);
    }

}
