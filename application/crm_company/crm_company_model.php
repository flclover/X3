<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  crm_company_model extends base_model {
	// 自动验证设置
	protected $_validate = array( array('name', 'require', '文件名必须', 1), array('content', 'require', '内容必须'), );
	protected $filter_config = array('skip' => 'acbc,abcd,defb', 'filter_abc' => 'name,sdfed,bcde,asdgt');

	function filter_abc($val) {
		if ($val == 'abc') {
			return 'abcabc';
		}
		return $val;
	}
}
?>