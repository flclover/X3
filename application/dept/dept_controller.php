<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/sef
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class dept_controller extends base_controller {
	private $auth_type = 'master';

	public function index() {
		$dept_list = model('dept') -> select('id,pid,name,is_del') -> order('sort asc') -> get_list();
		$dept_tree = list_to_tree($dept_list);
		$this -> assign('tree_menu', $dept_tree);

		$dept_list = model('dept') -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('dept_list', $dept_list);

		$where[] = array('is_del', 'eq', 0);
		$dept_grade_list = model('dept_grade') -> where($where) -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('dept_grade_list', $dept_grade_list);

		$this -> display();
	}

	public function add() {
		$where[] = array('is_del', 'eq', 0);
		$dept_grade_list = model('dept_grade') -> where($where) -> order('sort asc') -> get_field('id,name', true);
		$this -> assign('dept_grade_list', $dept_grade_list);

		$this -> display();
	}

	public function del($id) {
		$this -> _destory($id);
	}

	public function select_pid() {
		$where[] = array('is_del', 'eq', 0);
		$dept_list = model('dept') -> where($where) -> select('id,pid,name') -> order('sort asc') -> get_list();

		$dept_tree = list_to_tree($dept_list);
		$this -> assign('tree', $dept_tree);

		$root['id'] = 0;
		$root['name'] = '根节点';
		$root['child'] = $tree;

		$pid = array();
		$this -> assign('pid', $pid);

		$this -> display();
	}

	public function sync_to_wechat() {
		if (IS_AJAX) {
			$job_id = session('job_id');
			if (!empty($job_id)) {
				Vendor('WeChat.txl_api');
				$txl = new \TXL_API();
				sleep(1);
				echo $txl -> get_result($job_id);
				die ;
			} else {
				$tmp_file = tempnam(sys_get_temp_dir(), 'x');
				$fp = fopen($tmp_file, 'a');
				$title = array('部门名称', '部门ID', '父部门ID', '排序');
				fputcsv($fp, $title);

				$where['is_del'] = array('eq', 0);
				$dept_list = M('Dept') -> where($where) -> getField('name,id,pid,sort');
				foreach ($dept_list as $val) {
					if ($val['pid'] == 0) {
						$val['pid'] = 1;
					}
					fputcsv($fp, $val);
				}

				fclose($fp);
				rename($tmp_file, $tmp_file . ".csv");

				Vendor('WeChat.media_api');
				Vendor('WeChat.txl_api');

				$agent_id = get_system_config("helper_agent_id");

				$api = new \MEDIA_API($agent_id);
				$txl = new \TXL_API();
				//test entry
				$info = array();
				$info["media"] = '@' . $tmp_file . ".csv";

				$ret = $api -> uploadMedia($info, "image");

				if (!empty($ret['errcode'])) {
					echo $ret['errmsg'];
					die ;
				}
				$media_id = $ret['media_id'];

				$data['media_id'] = $media_id;
				$txl_ret = $txl -> replace_party($data);

				$job_id = json_decode($txl_ret, true);
				$job_id = $job_id['jobid'];
				$job_id = session('job_id', $job_id);
				unlink($tmp_file . ".csv");
				die ;
			}
		}
		session('job_id', null);
		$this -> display();
		die ;
	}

}
?>