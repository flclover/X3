<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class doc_controller extends base_controller {
	protected $app_type = 'folder';
	protected $auth_map = array('admin' => 'test1,test,del,move_to', 'read' => 'index,folder_manage');

	//过滤查询字段
	protected function _search_filter(&$map) {
		$map[] = array('doc.is_del', 'eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword) && empty($map['64'])) {
			$map[] = array('name', 'like', '%' . $keyword . '%');
		}
	}

	public function index() {
		$model = new doc_model();
		$model -> test();
		$map = $this -> _search($model);
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		if (!empty($model)) {
			$list = $this -> _list($model, $map);
		}
		$this -> display();
	}

	public function folder($fid) {
		$this -> assign('auth', $this -> auth);
		$auth = $this -> auth;

		$model = new model('doc');
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
		$map[] = array('folder', 'eq', $fid);
		if (!empty($model)) {
			$this -> _list($model, $map);
		}

		$where = array();
		$where[] = array('id', 'eq', $fid);
		$system_folder_model = new model('system_folder');
		$folder_name = $system_folder_model -> where($where) -> get_field("name");
		$this -> assign("folder_name", $folder_name);
		$this -> assign("folder", $fid);

		$this -> _assign_folder_list();
		$this -> display();
		return;
	}

	function add() {
		$this -> assign('folder', get('fid'));
		$this -> display();
	}

	function read($id) {
		$this -> edit($id);
	}

	function edit($id) {
		$where[] = array('id', 'eq', $id);
		$folder_id = model('doc') -> where($where) -> get_field('folder');

		$auth = model('system_folder') -> get_folder_auth($folder_id);
		$this -> assign("auth", $auth);

		$this -> _edit($id);
	}

	function del($id) {
		$where[] = array('id', 'in', $id);
		$doc_model = model('doc');
		$folder = array_unique($doc_model -> where($where) -> get_field('folder', true));
		if (count($folder) == 1) {
			$auth = model('system_folder') -> get_folder_auth($folder[0]);
			$auth -> admin = true;
			if ($auth -> admin == true) {
				$this -> _del($id);
			}
		} else {
			$return['info'] = "删除失败";
			$return['status'] = 0;
			ajax_return($return);
		}
	}

	public function move_to($id, $val) {
		$target_folder = $val;
		$where[] = array('id', 'in', $id);
		$doc_model = model('doc');

		$folder = array_unique($doc_model -> where($where) -> get_field('folder', true));

		if (count($folder) == 1) {
			$auth = model('system_folder') -> get_folder_auth($folder[0]);
			if ($auth -> admin == true) {
				$ret = $doc_model -> where($where) -> set_field('folder', $val);
				if ($ret !== false) {
					$return['info'] = "操作成功";
					$return['status'] = 1;
					ajax_return($return);
				} else {
					$return['info'] = "操作失败";
					$return['status'] = 1;
					ajax_return($return);
				}
			}
		} else {
			$return['info'] = "操作失败";
			$return['status'] = 0;
			ajax_return($return);
		}
	}

	function folder_manage() {
		$this -> _system_folder_manage('文档管理', true);
	}

	function upload() {
		$this -> _upload();
	}

	function down($sid) {
		$this -> _down($sid);
	}

}
