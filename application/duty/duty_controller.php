<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class duty_controller extends base_controller {
    protected $config = array('app_type' => 'master');

    public function index() {
        $model = new model('duty');
        $list = $model -> order('sort asc') -> get_list();
        $this -> assign('list', $list);
        $this -> display();
    }

    public function del() {
        $duty_id = post('id');
        $model = model("duty_user");
        $where[] = array('duty_id', 'eq', $duty_id);
        $model -> where($where) -> delete();
        $this -> _destory($duty_id);
    }

    public function user() {
        $keyword = request('keyword');

        $user_list = model('user') -> get_user_list($keyword);
        $this -> assign("user_list", $user_list);

        $where[] = array('is_del', 'eq', 0);
        $duty_list = model('duty') -> where($where) -> order('sort asc') -> get_list();
        $this -> assign("duty_list", $duty_list);

        $this -> display();
    }

    public function set_duty() {
        $user_list = post('user_id');
        $duty_list = post('duty_list');

        $model = model('duty');
        $model -> del_duty($user_list);

        $result = $model -> set_duty($user_list, $duty_list);
        if ($result === false) {
            $this -> error('操作失败！');
        } else {
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('操作成功！');
        }
    }

    public function get_duty_list() {
        $id = request('id');
        $where[] = array('user_id', 'eq', $id);
        $data = model('duty_user') -> where($where) -> get_list();
        if ($data !== false) {// 读取成功
            $return['data'] = $data;
            $return['status'] = 1;
            exit(json_encode($return));
        }
    }

}
?>