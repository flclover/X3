<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

// 角色模型
use sef\model;

class  duty_model extends base_model {

    public $_validate = array( array('name', 'require', '名称必须'), );

    function del_duty($user_list) {
        if (empty($user_list)) {
            return true;
        }
        if (is_array($user_list)) {
            $user_list = array_filter($user_list);
        } else {
            $user_list = explode(",", $user_list);
            $user_list = array_filter($user_list);
        }
        $user_list = implode(",", $user_list);

        if (empty($user_list)) {
            return true;
        }
        $table = $this -> table_prefix . 'duty_user';

        $result = $this -> db -> execute('delete from ' . $table . ' where user_id in (' . $user_list . ')');
        if ($result === false) {
            return false;
        } else {
            return true;
        }
    }

    function set_duty($user_list, $duty_list) {
        if (empty($user_list)) {
            return true;
        }
        if (is_array($user_list)) {
            $user_list = array_filter($user_list);
        } else {
            $user_list = array_filter(explode(",", $user_list));
        }
        $user_list = implode(",", $user_list);

        if (empty($user_list)) {
            return true;
        }
        if (is_array($duty_list)) {
            $duty_list = array_filter($duty_list);
        } else {
            $duty_list = array_filter(explode(",", $duty_list));
        }
        $duty_list = implode(",", $duty_list);

        $where = 'a.id in(' . $user_list . ') AND b.id in(' . $duty_list . ')';
        $sql = 'INSERT INTO ' . $this -> table_prefix . 'duty_user (user_id,duty_id)';
        $sql .= ' SELECT a.id, b.id FROM ' . $this -> table_prefix . 'user a, ' . $this -> table_prefix . 'duty b WHERE ' . $where;

        $result = $this -> execute($sql);
        return $result;
    }

    function get_sub_duty_user($user_id) {
        //查询都有那些业务角色
        $where1[] = array('user_id', 'eq', $user_id);
        $duty_list = model('duty_user') -> where($where1) -> get_field('duty_id', true);

        if (!empty($duty_list)) {
            //查询同样权限的所有用户
            $where2[] = array('duty_id', 'in', $duty_list);
            $user_list = model('duty_user') -> where($where2) -> get_field('user_id', true);

            //这些用户都有那些业务角色
            $where3[] = array('user_id', 'in', $user_list);
            $user_duty = model('duty_user') -> where($where3) -> get_field('duty_id', true);

            //查询还有其他权限的用户
            $diff_duty = array_diff($user_duty, $duty_list);
            if (!empty($diff_duty)) {
                $where4[] = array('duty_id', 'in', $diff_duty);
                $diff_user = model('duty_user') -> where($where4) -> get_field('user_id', true);
                return array_diff($user_list, $diff_user);
            } else {
                return $user_list;
            }
        } else {
            $all_user = model('user') -> get_field('id', true);
            $duty_user = model('duty_user') -> get_field('user_id', true);

            return array_diff($all_user, $duty_user);
        }
    }

}
?>