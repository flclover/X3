<?php
use sef\controller;
use sef\model;

class file_upload_controller extends controller {
    protected $app_type = 'widget';

    function add($name = null, $id = null) {
        if (empty($name)) {
            $name = "add_file";
        }
        if (empty($id)) {
            $id = $name;
        }
        $this -> assign('name', $name);
        $this -> assign('id', $id);
        echo $this -> fetch('add');
    }

    function edit($add_file, $name = null, $id = null) {
        if (empty($name)) {
            $name = "add_file";
        }
        if (!empty($add_file)) {
            $files = array_filter(explode(';', $add_file));
            $files = array_map('decrypt', $files);

            $where[] = array('id', 'in', $files);
            $model = new model('file');
            $file_list = $model -> where($where) -> get_list();

            $this -> assign('file_list', $file_list);

            if (empty($id)) {
                $id = $name;
            }
            $this -> assign('id', $id);
            $this -> assign('add_file', $add_file);
        }

        $this -> assign('name', $name);
        echo $this -> fetch('edit');
    }

    function view($add_file) {
        if (!empty($add_file)) {
            $files = array_filter(explode(';', $add_file));
            $files = array_map('decrypt', $files);

            $where['id'] = array('id', 'in', $files);
            $file_list = model('file') -> where($where) -> get_list();
            $this -> assign('file_list', $file_list);
            echo $this -> fetch('view');
        }
    }

    function link($add_file) {
        if (!empty($add_file)) {
            $files = array_filter(explode(';', $add_file));
            $files = array_map('decrypt', $files);

            $where[] = array('id', 'in', $files);
            $file_list = model('file') -> where($where) -> get_list();
        }
        $this -> assign('file_list', $file_list);
        $this -> display();
    }

    function html($add_file) {
        if (!empty($add_file)) {
            $files = array_filter(explode(';', $add_file));
            $files = array_map('decrypt', $files);

            $where[] = array('id', 'in', $files);

            $file_list = model('file') -> where($where) -> get_list();
            $data['file_list'] = $file_list;
            $this -> assign('add_file', $add_file);

            $this -> assign('file_list',$file_list);
            return $this -> fetch('view');
        }
    }

}
?>