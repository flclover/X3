<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use framework\model;

class  forum_model extends base_model {
	// 自动验证设置
	protected $_validate = array( array('name', 'require', '文件名必须', 1), array('content', 'require', '内容必须'), );
	protected $_view_fields = array();
	protected $filter = array('abc' => 'acbc,abcd,defb', 'abce' => 'abcde,sdfed,bcde,asdgt');

	function _before_insert(&$data, $options) {
		$sql = "SELECT CONCAT(year(now()),'-',LPAmodel(count(*)+1,4,0)) doc_no FROM `" . $this -> tablePrefix . "doc` WHERE 1 and year(FROM_UNIXTIME(create_time))>=year(now())";
		$rs = $this -> db -> query($sql);
		if ($rs) {
			$data['doc_no'] = $rs[0]['doc_no'];
		} else {
			$data['doc_no'] = date('Y') . "-0001";
		}
	}

	function get_index() {
		$this -> select('forum.*,user.pic pic');
		$this -> from('forum');
		$this -> inner('user', 'user.id=forum.user_id');
	}

	function get_index_list() {
		$this -> select('forum.*,user.pic pic');
		$this -> from('forum');
		$this -> inner('user', 'user.id=forum.user_id');
		$this -> limit(0, 10);
		return $this -> get_list();
	}

}
?>