<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class kf_reply_controller extends base_controller {
    //过滤查询字段
    protected function _search_filter(&$map) {

    }

    public function index() {
        $auth = $this -> auth;

        $this -> assign('auth', $auth);

        $model = new model('kf_reply');
        $map = $this -> _search();
        if (method_exists($this, '_search_filter')) {
            $this -> _search_filter($map);
        }
        if (!empty($model)) {
            $this -> _list($model, $map);
        }
        $this -> display();
        return;
    }

    function add() {
        $this -> assign('folder', get('fid'));
        $this -> display();
    }

    function read($id) {
        $this -> edit($id);
    }

    function del($id) {
        $this -> _destory($id);
    }

    public function move_to($id, $val) {
        $target_folder = $val;
        $where[] = array('id', 'in', $id);
        $doc_model = model('doc');

        $folder = array_unique($doc_model -> where($where) -> get_field('folder', true));

        if (count($folder) == 1) {
            $auth = model('system_folder') -> get_folder_auth($folder[0]);
            if ($auth -> admin == true) {
                $ret = $doc_model -> where($where) -> set_field('folder', $val);
                if ($ret !== false) {
                    $return['info'] = "操作成功";
                    $return['status'] = 1;
                    ajax_return($return);
                } else {
                    $return['info'] = "操作失败";
                    $return['status'] = 1;
                    ajax_return($return);
                }
            }
        } else {
            $return['info'] = "操作失败";
            $return['status'] = 0;
            ajax_return($return);
        }
    }

    function folder_manage() {
        $this -> _system_folder_manage('文档管理', true);
    }

    function upload() {
        $this -> _upload();
    }

    function down($sid) {
        $this -> _down($sid);
    }

}
