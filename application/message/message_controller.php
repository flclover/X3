<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class message_controller extends base_controller {
	protected $app_type = 'folder';
	protected $auth_map = array('admin' => 'del,move_to,folder_manage');

	protected function _init_filter() {
		$this -> filter_config['app'] = 'field5,field5,field6,field7';
		$this -> filter_config['fliter_script'] = 'field5,field5,field6,field7';
		$this -> filter_config['clean_xss'] = 'field5,field5,field6,field7';
	}

	//过滤查询字段
	protected function _search_filter(&$map) {
		$map['is_del'] = array('eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword) && empty($map['64'])) {
			$map['name'] = array('like', "%" . $keyword . "%");
		}
	}

	public function index() {
		$model = new doc_model();
		$model -> test();

		$map = array();
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	public function edit($id) {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);

		$model = M("Doc");
		$folder_id = $model -> where("id=$id") -> get_field('folder');
		$this -> assign("auth", model("SystemFolder") -> get_folder_auth($folder_id));
		$this -> _edit($id);
	}

	public function folder($fid) {
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$this -> assign('auth', $this -> auth);

		$model = model("Doc");
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$map['folder'] = $fid;

		if (!empty($model)) {
			$this -> _list($model, $map);
		}

		$where = array();
		$where['id'] = array('eq', $fid);

		$folder_name = M("SystemFolder") -> where($where) -> get_field("name");
		$this -> assign("folder_name", $folder_name);
		$this -> assign("folder", $fid);

		$this -> _assign_folder_list();
		$this -> display();
		return;
	}

	public function add($fid) {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);

		$this -> assign('folder', $fid);
		$this -> display();
	}

	public function read($id) {

		$model = M("Doc");
		$where[] = array('id', 'eq', $id);

		$folder_id = $model -> where($where) -> get_field('folder');

		$this -> assign("auth", model("system_folder") -> get_folder_auth($folder_id));

		$this -> _edit($id);
	}

	public function del($id) {
		$where['id'] = array('in', $id);
		$folder = M("Doc") -> distinct(true) -> where($where) -> get_field('folder', true);
		if (count($folder) == 1) {
			$auth = model("SystemFolder") -> get_folder_auth($folder[0]);
			if ($auth->admin == true) {
				$this -> _del($id);
			}
		} else {
			$return['info'] = "删除失败";
			$return['status'] = 0;
			ajax_return($return);
		}
	}

	public function move_to($id, $val) {
		$target_folder = $val;
		$where['id'] = array('in', $id);
		$folder = M("Doc") -> distinct(true) -> where($where) -> get_field('folder', true);
		if (count($folder) == 1) {
			$auth = model("SystemFolder") -> get_folder_auth($folder[0]);
			if ($auth->admin == true) {
				$field = 'folder';
				$result = $this -> _set_field($id, $field, $target_folder);

				if ($result) {
					$return['info'] = "操作成功";
					$return['status'] = 1;
					ajax_return($return);
				} else {
					$return['info'] = "操作失败";
					$return['status'] = 1;
					ajax_return($return);
				}
			}
		} else {
			$return['info'] = "操作成功";
			$return['status'] = 1;
			ajax_return($return);
		}
	}

	function folder_manage() {
		$this -> _system_folder_manage('文档管理', true);
	}

	function upload() {
		$this -> _upload();
	}

	function down($attach_id) {
		$this -> _down($attach_id);
	}

}
