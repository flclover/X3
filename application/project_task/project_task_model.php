<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  project_task_model extends base_model {

	function get_member($project_id) {
		if (!empty($project_id)) {
			$where_member[] = array('project_id', 'eq', $project_id);
			$member_list = model('project_member') -> where($where_member) -> get_field('user_id', true);

			if (!empty($member_list)) {
				$where_user[] = array('id', 'in', $member_list);
			} else {
				$where_user['_string'] = '1=2';
			}
			$member_list = model('user') -> where($where_user) -> get_field('id,name');
		}
		return $member_list;
	}

	function set_member($member, $project_id) {
		$project_model = model('project_member');

		$where_del[] = array('project_id', 'eq', $project_id);
		$project_model -> where($where_del) -> delete();

		$member_list = json_decode($member, true);

		$data['project_id'] = $project_id;
		foreach ($member_list as $val) {
			$data['user_id'] = $val['data'];
			$project_model -> add($data);
		}
	}

	function get_executor($task_id) {
		if (!empty($task_id)) {
			$where_executor['task_id'] = array('eq', $task_id);
			$executor_list = M('ProjectTaskExecutor') -> getField('user_id', true);
			if (!empty($executor_list)) {
				$where_user['id'] = array('in', $executor_list);
				$executor_list = M('User') -> where($where_user) -> getField('id,name');
			} else {
				return;
			}
		}
		return $executor_list;
	}

	function set_executor($executor, $task_id) {
		$where_del['task_id'] = array('eq', $task_id);
		M("ProjectTaskExecutor") -> where($where_del) -> delete();

		$$executor_list = array_filter(explode(';', $executor));
		$data['task_id'] = $task_id;
		foreach ($$executor_list as $val) {
			$temp = explode('|', $val);
			$data['user_id'] = $temp[1];
			M("ProjectTaskExecutor") -> add($data);
		}
	}

	function set_workload($project_id) {
		$where_all['project_id'] = array('eq', $project_id);
		$all = model('project_task') -> where($where_all) -> sum('workload');

		$where_finish['project_id'] = array('eq', $project_id);
		$where_finish['is_finish'] = array('eq', 2);
		$finish = model('project_task') -> where($where_finish) -> sum('workload');

		$data['all'] = $all;
		$data['yes'] = $finish;
		$data['id'] = $project_id;

		model('project') -> save($data);
	}

	function calc_finish_rate($project_id) {
		$project_task_model = model('project_task');
		$where_all[] = array('project_id', 'eq', $project_id);
		$where_all[] = array('is_del', 'eq', 0);
		$all = $project_task_model -> where($where_all) -> get_field("sum('workload')");

		$where_finish[] = array('project_id', 'eq', $project_id);
		$where_finish[] = array('is_finish', 'eq', 4);
		$where_finish[] = array('is_del', 'eq', 0);
		$finish = $project_task_model -> where($where_finish) -> get_field("sum('workload')");

		if (!empty($all)) {
			$data['finish_rate'] = round($finish / $all, 2) * 100;
			$data['id'] = $project_id;
			model('project') -> save($data);
		}
	}

	function get_data($return_data = false) {
		$this -> select('project_task.*,project.name project_name');
		$this -> from('project_task');
		$this -> inner('project', 'project_task.project_id=project.id');
		if ($return_data) {
			return $this -> get_list();
		} else {
			return $this;
		}
	}

}
?>