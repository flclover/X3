<?php

use sef\controller;
use sef\model;
use weixin\mp;

class public_controller extends controller {

	public function index() {
		import('verify_code/verify_code');
		$vc = new verify_code();
		$vc -> create_image();
		$_SESSION['authnum_session'] = $vc -> get_code();
		//验证码保存到SESSION中
	}

	public function online() {
		import('weixin/mp/helper');
		import('weixin/mp/auth');

		$auth = new mp\auth();

		$redirect_uri = $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER['HTTP_HOST'] . '/' . url('get_base_info');
		echo $redirect_uri;
		echo '<br>';
		$url = $auth -> get_redirect_url($redirect_uri);
		echo $url;
		echo '<img src="' . url('qrcode', array('str' => $url)) . '">';
	}

	public function get_base_info() {
		$code = request('code');
		echo $code;
		echo 222;
		import('weixin/mp/helper');
		import('weixin/mp/auth');

		$auth = new mp\auth();

		$redirect_uri = url('get_base_info');
		$url = $auth -> get_redirect_url($redirect_uri);

	}

	public function qrcode($str) {
		vendor();
		$qrCode = new Endroid\QrCode\QrCode();
		//$str = "http://car.smeoa.com/index.php?app=car&qrcode=" . $qrcode;
		//echo $str;
		$qrCode -> setText($str);
		$qrCode -> setSize(300);
		$qrCode -> setPadding(10);
		$qrCode -> render();
		die ;
	}

	public function news_index() {
		$model = model('news');
		$model -> test();
		$map[] = array('news.is_del', 'eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$map[] = array('name', 'like', $keyword);
		}

		if (!empty($model)) {
			$list = $this -> _list($model, $map);
		}

		$where[] = array('controller', 'eq', 'news');
		$left_menu = model('system_folder') -> where($where) -> get_list();
		$this -> assign('left_menu', $left_menu);

		$this -> display();
	}

	public function news_read($id) {
		$model = new model('news');
		$where[] = array('id', 'eq', $id);
		$vo = $model -> where($where) -> find($id);
		$this -> assign('vo', $vo);
		$this -> display();
	}

	public function news_folder($fid) {
		$this -> assign('auth', $this -> auth);
		$auth = $this -> auth;

		$model = new model('news');
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$map[] = array('name', 'like', $keyword);
		}

		$map[] = array('news.is_del', 'eq', '0');
		$map[] = array('folder', 'eq', $fid);
		if (!empty($model)) {
			$this -> _list($model, $map);
		}

		$where = array();
		$where[] = array('id', 'eq', $fid);
		$system_folder_model = new model('system_folder');
		$folder_name = $system_folder_model -> where($where) -> get_field("name");
		$this -> assign("folder_name", $folder_name);
		$this -> assign("folder", $fid);

		$where_left_menu[] = array('controller', 'eq', 'news');
		$left_menu = model('system_folder') -> where($where_left_menu) -> get_list();
		$this -> assign('left_menu', $left_menu);

		$this -> display();
	}

	protected function _list($model, $map, $sort = '') {
		//排序字段 默认为主键名
		if (isset($_REQUEST['_sort'])) {
			$sort = $_REQUEST['_sort'];

		} elseif (empty($sort)) {
			$sort = "id desc";
		}
		$count_model = clone $model;
		//取得满足条件的记录数
		$count = $count_model -> where($map) -> count();
		if ($count > 0) {
			//创建分页对象
			if (!empty($_REQUEST['list_rows'])) {
				$list_rows = $_REQUEST['list_rows'];
			} else {
				$list_rows = get_user_config('list_rows');
			}
			import("page/page");
			$p = new \page($count, $list_rows);

			$vo_list = $model -> where($map) -> limit($p -> first_row, $list_rows) -> order($sort) -> get_list();
			//分页显示
			$page = $p -> show();
			if ($vo_list) {
				$this -> assign('list', $vo_list);
				$this -> assign('sort', $sort);
				$this -> assign("page", $page);
				return $vo_list;
			}
		}
		return FALSE;
	}

	public function test22() {
		set_time_limit(0);
		error_reporting(E_ALL ^ E_NOTICE);

		import('phpQuery/phpQuery');

		$model = model('qdhr');
		for ($i = 10001; $i <= 90001; $i++) {

			$doc = phpQuery::newDocumentFile("http://www.qdhr.net/Qdhr/Companys/company_$i/");

			$company_name = $doc -> find('.company_text .name') -> text();

			if (empty($company_name)) {
				continue;
			}

			$company_address = $doc -> find('.company_text .add') -> text();
			$company_info = $doc -> find('.company_text .info') -> text();

			$doc = phpQuery::newDocumentFile("http://www.qdhr.net/Qdhr/Companys/company_$i/companyInfo.html");
			//$contact = $doc -> find('.company_text') ->text();

			$data['name'] = $company_name;
			$data['address'] = $company_address;
			$data['info'] = $company_info;
			$data['contact'] = $doc;
			$model -> add($data);
		}

		$doc -> find('.company_text') -> text();
	}

	public function test() {
		set_time_limit(0);
		$list = model('qdhr') -> get_list();
		foreach ($list as $val) {

			$str = strip_tags($val['info']);

			$search = '/10人以下/';
			$isMatched = preg_match($search, $str, $matches);
			if ($isMatched) {
				continue;
			}

			$str = strip_tags($val['contact']);

			$search = '/(\d{11})/';
			$isMatched = preg_match($search, $str, $matches);
			if ($isMatched) {
				$tel = $matches[1];
			} else {
				continue;
			}

			//$str = ' 15812345678';
			// echo $str;
			$search = '/联系人：(.*)电/';
			$isMatched = preg_match($search, $str, $matches);
			if ($isMatched) {
				$name = $matches[1];
			}
			$vcf = '';
			$vcf .= "BEGIN:VCARD" . PHP_EOL;
			$vcf .= "VERSION:3.0" . PHP_EOL;
			$vcf .= "N:" . mb_substr($name, 0, 1) . ";" . mb_substr($name, 1) . ";;;" . PHP_EOL;
			$vcf .= "FN:" . $name . PHP_EOL;
			$vcf .= "TEL;TYPE=mobile:" . $tel . PHP_EOL;
			$vcf .= "ORG:" . trim($val['name']) . PHP_EOL;
			$vcf .= "NOTE:" . trim($val['info']) . PHP_EOL;
			$vcf .= "END:VCARD" . PHP_EOL;

			file_put_contents("1.vcf", $vcf, FILE_APPEND);
			//print_r($matches);
		}
	}

	public function login() {

		$this -> assign("login_verify_code", get_system_config("login_verify_code"));

		$auth_id = session(config('user_auth_key'));

		if (!isset($auth_id)) {
			$this -> display();
		} else {
			header('Location: ' . url('index/index'));
		}
	}

	// 登录检测
	public function check_login() {
		$login_verify_code = get_system_config("login_verify_code");
		if (!empty($login_verify_code)) {
			if (post('verify_code') != session('verify_code')) {
				$this -> error('验证码或密码错误！');
			}
		}

		$emp_no = post('emp_no');
		$password = post('password');
		if (empty($emp_no)) {
			$this -> error('帐号必须！');
		} elseif (empty($password)) {
			$this -> error('密码必须！');
		}

		$map = array();
		$map[] = array('emp_no', 'eq', post('emp_no'));
		$map[] = array('is_del', 'eq', 0);
		$map[] = array('password', 'eq', md5($_POST['password']));

		$model = new user_model();
		$auth_info = $model -> where($map) -> find();
		//使用用户名、密码和状态的方式进行认证
		if ($auth_info == false) {
			$this -> error('帐号或密码错误！');
		} else {
			session(config('user_auth_key'), $auth_info['id']);
			session('emp_no', $auth_info['emp_no']);
			session('user_name', $auth_info['name']);
			session('user_pic', $auth_info['pic']);
			session('dept_id', $auth_info['dept_id']);
			session('dept_name', get_dept_name($auth_info['id']));
			session('sub_dept', get_sub_dept($auth_info['dept_id']));
			session('position_id', $auth_info['position_id']);

			$data = array();
			$data['id'] = $auth_info['id'];
			$data['last_login_ip'] = get_client_ip();
			$model -> save($data);
			cookie('top_menu_id', 255);
			header('Location: ' . url('kf/index'));
		}
	}

	/* 退出登录 */
	public function logout() {
		$auth_id = session(config('user_auth_key'));
		if (isset($auth_id)) {
			session(null);
			$this -> assign("jump_url", url('public/login'));
			$this -> success('退出成功！');
		} else {
			$this -> assign("jump_url", url('public/login'));
			$this -> error('退出成功！');
		}
	}

	public function verify() {
		import('verify_code/verify_code');
		$vc = new verify_code();
		$vc -> create_image();
		session('verify_code', $vc -> get_code());
	}

}
