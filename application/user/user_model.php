<?php
use framework\model;
class  user_model extends base_model {
    function get_user_info($id) {
        $this -> select('user.*,position.name position_name,dept.name dept_name');
        $this -> from('user');
        $this -> inner('position', 'position.id=user.position_id');
        $this -> inner('dept', 'dept.id=user.dept_id');

        $where[] = array('user.id', 'eq', $id);
        return $this -> where($where) -> find();
    }

    function get_user_list($keyword) {
        $this -> select('user.*,position.name position_name,dept.name dept_name');
        $this -> from('user');
        $this -> inner('position', 'position.id=user.position_id');
        $this -> inner('dept', 'dept.id=user.dept_id');

        $where[] = array('user.name', 'like', $keyword, 'or');
        $where[] = array('dept.name', 'like', $keyword, 'or');
        $where[] = array('position.name', 'like', $keyword, 'or');         
        $map[] = array('complex', $where);

        $result = $this -> where($map) -> get_list();
        return $result;
    }

}
?>