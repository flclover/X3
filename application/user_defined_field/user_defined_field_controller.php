<?php
use sef\controller;
use sef\model;

class user_defined_field_controller extends base_controller {

	protected $config = array('app_type' => 'public');

	public function edit($data) {
		//附件类型时处理数据
		if ($data['type'] == 'add_file') {
			$file_list = array();
			if (!empty($data['val'])) {
				$files = array_filter(explode(';', $data['val']));
				$files = array_map(think_decrypt, $files);
				$where['id'] = array('in', $files);
				$file_list = M("File") -> where($where) -> select();
			}
			$this -> assign('file_list', $file_list);
		}
		if ($data['type'] == 'popup') {
			$data['data'] = strtolower($data['data']);
		}
		$data['readonly'] = false;
		$this -> assign('data', $data);
		$layout = $data['layout'];
		return $this -> fetch('user_defined_field/view/' . $layout . '.html');
	}

	public function view($data) {
		if ($data['type'] == 'add_file') {
			$file_list = array();
			if (!empty($data['val'])) {
				$files = array_filter(explode(';', $data['val']));
				$files = array_map(think_decrypt, $files);
				$where['id'] = array('in', $files);
				$file_list = M("File") -> where($where) -> select();
			}
			$this -> assign('file_list', $file_list);
		}

		$data['readonly'] = true;
		$this -> assign('data', $data);

		$layout = $data['layout'];
		$content = $this -> display('user_defined_field/' . $layout);
	}

	public function udf_read($udf_data, $tpl) {
		$field_data = json_decode($udf_data, true);
		// dump($field_data);
		foreach ($field_data as $key => $val) {
			$data[] = $val;
		}
		$this -> assign(data, $data) -> display("UdfTpl:$tpl");
	}

	function conv_data($val) {
		$new = array();
		if (strpos($val, "|") !== false) {
			$arr_tmp = explode(",", $val);
			foreach ($arr_tmp as $item) {
				$tmp = explode("|", $item);
				$new[$tmp[0]] = $tmp[1];
			}
		} else {
			$new = $val;
		}
		return $new;
	}

}
?>