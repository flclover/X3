<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class work_log_controller extends base_controller {

	protected $config = array('app_type' => 'common', 'read' => 'del_comment,index2,good,get_work_log,user_list,audit,set_audit,get_user_list,sub,comment_me,reply_me', 'write' => 'comment,reply,content_item,plan_item', 'admin' => 'all');
	//过滤查询字段
	function _search_filter(&$map) {
		$map[] = array('is_del', 'eq', '0');
		if (!empty($_POST['keyword'])) {
			$where[] = array('content', 'like', post('keyword'), 'or');
			$where[] = array('user_name', 'like', post('keyword'), 'or');
			$map['_complex'] = $where;
		}
	}

	public function index($type = 0) {
		$this -> assign('user_id', get_user_id());

		$model = model("work_log");
		$map = $this -> _search($model);
		$map[] = array('user_id', 'eq', get_user_id());

		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	function sub() {
		$this -> assign('user_id', get_user_id());

		$where = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($where);
		}

		$dept_id = get_dept_id();
		$sub_dept = get_sub_dept($dept_id);

		$sub_duty_user_list = model('duty') -> get_sub_duty_user(get_user_id());
		$sub_position_user_list = model('position') -> get_sub_position_user(get_user_id());

		if (empty($sub_duty_user_list) or empty($sub_position_user_list)) {
			$where[] = array('string', '1=2');
		} else {
			$user_list = array_intersect($sub_duty_user_list, $sub_position_user);
			if (!empty($user_list)) {
				$where[] = array('user_id', 'in', $user_list);
			}
			if (!empty($user_list)) {
				$where[] = array('dept_id', 'in', $sub_dept);
			}
		}

		$model = model("work_log");

		if (!empty($model)) {
			$this -> _list($model, $where);
		}
		$this -> display();
	}

	function comment_me() {
		$this -> assign('user_id', get_user_id());

		$model = model("work_log");
		$map = $this -> _search($model);
		$map[] = array('user_id', 'eq', get_user_id());

		$where_log[] = array('user_id', 'eq', get_user_id());
		$log_list = model('work_log') -> where($where_log) -> get_field('id', true);

		if (!empty($log_list)) {
			$where_comment[] = array('row_id', 'in', $log_list);
			$where_comment[] = array('controller', 'eq', 'work_log');
			$where_comment[] = array('user_id', 'neq', get_user_id());
			$row_id_list = model('comment') -> where($where_comment) -> get_field('row_id', true);

			if (!empty($row_id_list)) {
				$row_id_list = array_unique($row_id_list);
				$map[] = array('id', 'in', $row_id_list);
			} else {
				$map[] = array('string', '1=2');
			}
		} else {
			$map[] = array('string', '1=2');
		}

		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	function reply_me() {
		$this -> assign('user_id', get_user_id());

		$model = model("work_log");
		$map = $this -> _search($model);

		$where_comment[] = array('row_type', 'eq', 'work_log');
		$where_comment[] = array('reply_to_user_id', 'eq', get_user_id());
		$row_id_list = model('comment') -> where($where_comment) -> get_field('row_id', true);

		if (!empty($row_id_list)) {
			$row_id_list = array_unique($row_id_list);
			$map[] = array('id', 'in', $row_id_list);
		} else {
			$map[] = array('_string', '1=2');
		}

		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$this -> assign('type', $type);
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	function get_work_log($id) {
		$list = model("work_log") -> find($id);
		$list['html'] = widget('file_upload/html', array('add_file' => $list['add_file']));

		$where_comment[] = array('row_id', 'eq', $id);
		$where_comment[] = array('controller', 'eq', 'work_log');
		$comment = model('comment') -> where($where_comment) -> get_list();
		$list['comment'] = $comment;
		ajax_return($list);
	}

	function good($id) {
		$where[] = array('id', 'eq', $id);
		$good = model('work_log') -> where($where) -> get_field('good');
		if (!empty($good)) {
			$good = json_decode($good, true);
		} else {
			$good = array();
		}
		$user_id = get_user_id();
		if (in_array($user_id, array_keys($good))) {
			unset($good[$user_id]);
			$return['status'] = 0;
		} else {
			$good[$user_id] = get_user_name();
			$return['status'] = 1;
		}
		$data['good'] = json_encode($good, JSON_UNESCAPED_UNICODE);
		model('work_log') -> where($where) -> save($data);
		$return['data'] = $good;
		ajax_return($return);
	}

	function comment($row_id) {
		$model = model("comment");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> type = 'comment';
		$model -> user_id = get_user_id();
		$model -> user_name = get_user_name();
		$model -> create_time = time();
		$model -> controller = APP_NAME;
		$list = $model -> add();

		$where[] = array('row_id', 'eq', $row_id);
		$where[] = array('controller', 'eq', APP_NAME);
		$comment = model('comment') -> where($where) -> get_list();

		if ($list !== false) {
			$return['stauts'] = 1;
			$return['comment'] = $comment;
			$return['info'] = '评论成功';
		} else {
			$return['stauts'] = 0;
			$return['info'] = '评论失败';
		}
		ajax_return($return);
	}

	function reply($row_id) {
		$model = model("Comment");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> type = 'reply';
		$model -> user_id = get_user_id();
		$model -> user_name = get_user_name();
		$model -> create_time = time();
		$model -> controller = APP_NAME;
		$list = $model -> add();

		$where[] = array('row_id', 'eq', $row_id);
		$where[] = array('controller', 'eq', APP_NAME);
		$comment = model('comment') -> where($where) -> get_list();

		if ($list !== false) {
			$return['stauts'] = 1;
			$return['comment'] = $comment;
			$return['info'] = '回复成功';
		} else {
			$return['stauts'] = 0;
			$return['info'] = '回复失败';
		}
		ajax_return($return);
	}

	function del_comment($comment_id) {
		$this -> _destory($comment_id, 'comment');
	}

	function upload() {
		$this -> _upload();
	}

	function down($attach_id) {
		$this -> _down($attach_id);
	}

	function del($id) {
		$this -> _del($id);
	}

}
