<?php
namespace sef;
class config {
	public static $data = array();

	function __construct() {

	}

	function __set($key, $val) {
		self::$data[$key] = $val;
	}

	function __get($key) {
		if (isset(self::$data[$key])) {
			return self::$data[$key];
		} else {
			return null;
		}
	}

	public function has($key) {
		return array_key_exists($key, self::$data);
	}
}

$config = new config();
