<?php
namespace sef;

class filter {
	static $method = array('clean_xss', 'check_str', 'app', 'abce', 'abcfe', 'badfe', 'badfea');

	static function start($config) {		
		foreach ($_REQUEST as $key => &$val) {
			foreach ($config as $function => $fields) {
				$arr_field = array_filter(explode(',', $fields));
				if (in_array($function, self::$method) && in_array($function, $arr_field)) {
					if (is_array($val)) {
						$sub_keys = array_keys($val);
						foreach ($sub_keys as $sub_key) {
							$val[$sub_key] = self::$function($val[$sub_key]);
						}
					} else {
						$val = self::$function($val);
					}
				}
			}
		}
	}

	static function clean_xss(&$string, $low = false) {
		if (!is_array($string)) {
			$string = trim($string);
			$string = strip_tags($string);
			$string = htmlspecialchars($string);
			if ($low) {
				return true;
			}
			$string = str_replace(array('"', "\\", "'", "/", "..", "../", "./", "//"), '', $string);
			$no = '/%0[0-8bcef]/';
			$string = preg_replace($no, '', $string);
			$no = '/%1[0-9a-f]/';
			$string = preg_replace($no, '', $string);
			$no = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';
			$string = preg_replace($no, '', $string);
			return true;
		}
		$keys = array_keys($string);
		foreach ($keys as $key) {
			clean_xss($string[$key]);
		}
	}

	//安全过滤输入[jb]
	static function check_str($string, $isurl = false) {
		$string = preg_replace('/[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]/', '', $string);
		$string = str_replace(array("\0", "%00", "\r"), '', $string);
		empty($isurl) && $string = preg_replace("/&(?!(#[0-9]+|[a-z]+);)/si", '&', $string);
		$string = str_replace(array("%3C", '<'), '<', $string);
		$string = str_replace(array("%3E", '>'), '>', $string);
		$string = str_replace(array('"', "'", "\t", ' '), array('“', '‘', ' ', ' '), $string);
		return trim($string);
	}

	static function app_name($string) {
		if (preg_match("/^[a-zA-Z0-9\_]+$/", $string)) {
			return true;
		}
		return false;
	}

	/**
	 * 安全过滤类-过滤javascript,css,iframes,object等不安全参数 过滤级别高
	 *  Controller中使用方法：$this->controller->fliter_script($value)
	 * @param  string $value 需要过滤的值
	 * @return string
	 */
	static function fliter_script($value) {
		$value = preg_replace("/(javascript:)?on(click|load|key|mouse|error|abort|move|unload|change|dblclick|move|reset|resize|submit)/i", "&111n\\2", $value);
		$value = preg_replace("/(.*?)<\/script>/si", "", $value);
		$value = preg_replace("/(.*?)<\/iframe>/si", "", $value);
		$value = preg_replace("//iesU", '', $value);
		return $value;
	}

	/**
	 * 安全过滤类-过滤HTML标签
	 *  Controller中使用方法：$this->controller->fliter_html($value)
	 * @param  string $value 需要过滤的值
	 * @return string
	 */
	static function fliter_html($value) {
		if (function_exists('htmlspecialchars'))
			return htmlspecialchars($value);
		return str_replace(array("&", '"', "'", "<", ">"), array("&", "\"", "'", "<", ">"), $value);
	}

	/**
	 * 安全过滤类-对进入的数据加下划线 防止SQL注入
	 *  Controller中使用方法：$this->controller->fliter_sql($value)
	 * @param  string $value 需要过滤的值
	 * @return string
	 */
	static function fliter_sql($value) {
		$sql = array("select", 'insert', "update", "delete", "\'", "\/\*", "\.\.\/", "\.\/", "union", "into", "load_file", "outfile");
		$sql_re = array("", "", "", "", "", "", "", "", "", "", "", "");
		return str_replace($sql, $sql_re, $value);
	}

	/**
	 * 安全过滤类-通用数据过滤
	 *  Controller中使用方法：$this->controller->fliter_escape($value)
	 * @param string $value 需要过滤的变量
	 * @return string|array
	 */
	static function fliter_escape($value) {
		if (is_array($value)) {
			foreach ($value as $k => $v) {
				$value[$k] = self::fliter_str($v);
			}
		} else {
			$value = self::fliter_str($value);
		}
		return $value;
	}

	/**
	 * 安全过滤类-字符串过滤 过滤特殊有危害字符
	 *  Controller中使用方法：$this->controller->fliter_str($value)
	 * @param  string $value 需要过滤的值
	 * @return string
	 */
	static function fliter_str($value) {
		$badstr = array("\0", "%00", "\r", '&', ' ', '"', "'", "<", ">", "   ", "%3C", "%3E");
		$newstr = array('', '', '', '&', ' ', '"', '\'', "<", ">", "   ", "<", ">");
		$value = str_replace($badstr, $newstr, $value);
		$value = preg_replace('/&((#(\d{3,5}|x[a-fA-F0-9]{4}));)/', '&\\1', $value);
		return $value;
	}

	/**
	 * 私有路劲安全转化
	 *  Controller中使用方法：$this->controller->filter_dir($fileName)
	 * @param string $fileName
	 * @return string
	 */
	static function filter_dir($fileName) {
		$tmpname = strtolower($fileName);
		$temp = array(':/', "\0", "..");
		if (str_replace($temp, '', $tmpname) !== $tmpname) {
			return false;
		}
		return $fileName;
	}

	/**
	 * 过滤目录
	 *  Controller中使用方法：$this->controller->filter_path($path)
	 * @param string $path
	 * @return array
	 */
	static public function filter_path($path) {
		$path = str_replace(array("'", '#', '=', '`', '$', '%', '&', ';'), '', $path);
		return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/', '/', $path), '/');
	}

	/**
	 * 过滤PHP标签
	 *  Controller中使用方法：$this->controller->filter_phptag($string)
	 * @param string $string
	 * @return string
	 */
	static public function filter_phptag($string) {
		return str_replace(array(''), array('<?', '?>'), $string);
	}

	/**
	 * 安全过滤类-返回函数
	 *  Controller中使用方法：$this->controller->str_out($value)
	 * @param  string $value 需要过滤的值
	 * @return string
	 */
	static public function str_out($value) {
		$badstr = array("<", ">", "%3C", "%3E");
		$newstr = array("<", ">", "<", ">");
		$value = str_replace($newstr, $badstr, $value);
		return stripslashes($value);
		//下划线
	}

}
