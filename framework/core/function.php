<?php

use sef\model;
use sef\controller;

function T($var) {
    print_r($var);
    die ;
}

function encrypt($string = '', $skey = 'xiaowei') {
    $str = md5($string) . md5(md5($string));
    $start = substr(ord($str), -1) + 1;
    $str_arr = str_split(base64_encode($string));
    $str_count = count($str_arr);
    foreach (str_split(md5($skey)) as $key => $val) {
        if ($key < $str_count) {
            $str_arr[$key] .= $val;
        }
    }
    $first = substr($str, 0, $start);
    $mid = str_replace(array('=', '+', '/'), array('-a', '-b', '-c'), join('', $str_arr));
    $end = substr('0' . dechex(strlen($mid)), -2);
    return substr($first . $mid . substr($str, $start), 0, 32) . $end;
}

function decrypt($string = '', $skey = 'xiaowei') {
    $start = substr(ord($string), -1) + 1;
    $end = hexdec(substr($string, -2));
    $str = substr($string, $start, $end);

    $str_arr = str_split(str_replace(array('-a', '-b', '-c'), array('=', '+', '/'), $str), 2);
    $str_count = count($str_arr);
    foreach (str_split(md5($skey)) as $key => $val)
        if ($key <= $str_count && isset($str_arr[$key]) && isset($str_arr[$key][1]) && $str_arr[$key][1] === $val) {
            $str_arr[$key] = $str_arr[$key][0];
        }
    return base64_decode(join('', $str_arr));
}

function model($model_name) {
    $class_name = $model_name . '_model';
    $model_path = APP_PATH . $model_name . '/' . $class_name . '.php';
    if (is_file($model_path)) {
        include_once $model_path;
        return new $class_name();
    } else {
        return new model($model_name);
    }
}

function ajax_return($val) {
    header("Content-Type:text/html; charset=utf-8");
    exit(json_encode($val));
}

function config($key, $val = null) {
    $config = new sef\config();
    if (!isset($val)) {
        return $config -> $key;
    } else {
        $config -> $val = $val;
    }
}

function widget($name, $vars = array()) {
    list($app, $method) = explode('/', $name);
    $app_class = $app . '_controller';
    $app = new $app_class;
    return call_user_func_array(array($app, $method), $vars);
}

function url($path, $vars = null) {
    $arr_path = array_filter(explode('/', $path));
    $count = count($arr_path);

    if ($count == 1) {
        $url = 'index.php?app=' . APP_NAME . '&method=' . $path;
    }
    if ($count == 2) {
        list($app, $method) = $arr_path;
        $url = 'index.php?app=' . $app . '&method=' . $method;
    }
    if (!empty($vars)) {
        if (is_array($vars)) {
            $url .= '&' . http_build_query($vars);
        }
        if (is_string($vars)) {
            $url .= '&' . $vars;
        }
    }
    return $url;
}

function cache($key, $val = null) {
    $cache = new sef\cache();
    if (!isset($val)) {
        echo 'y1';
        return $cache -> get($key);
    } else {
        $cache -> set($key, $val);
    }
}

function session($key, $val = null) {
    if ($key == null) {
        $_SESSION = array();
        return;
    }
    if (!isset($_SESSION)) {
        session_start();
    }
    if (isset($val)) {
        $_SESSION[$key] = $val;
    } else {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }
}

function cookie($key, $val = null) {
    $cookie_prefix = config('cookie_prefix');
    $key = $cookie_prefix . $key;
    if (isset($val)) {
		setcookie($key,$val,time()+3600,"/");
    } else {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        } else {
            return false;
        }
    }
}

function request($key) {
    if (isset($_REQUEST[$key])) {
        return $_REQUEST[$key];
    } else {
        return '';
    }
}

function get($key) {
    if (isset($_GET[$key])) {
        return $_GET[$key];
    } else {
        return '';
    }
}

function post($key) {
    if (isset($_POST[$key])) {
        return $_POST[$key];
    } else {
        return '';
    }
}

function redirect($url) {
    header('Location: ' . $url);
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function get_client_ip($type = 0, $adv = false) {
    $type = $type ? 1 : 0;
    static $ip = NULL;
    if ($ip !== NULL)
        return $ip[$type];
    if ($adv) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos)
                unset($arr[$pos]);
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u", ip2long($ip));
    $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}

function import($class) {
    include_once EXT_PATH . $class . '.php';
}

function vendor() {
    include_once FRAMEWORK_PATH . DS . 'vendor/autoload.php';
}

function filter_search_field($v1) {
    if ($v1 == "keyword")
        return true;
    $prefix = substr($v1, 0, 3);
    $arr_key = array("be_", "en_", "eq_", "li_", "lt_", "gt_", "bt_");
    if (in_array($prefix, $arr_key)) {
        return true;
    } else {
        return false;
    }
}
?>
