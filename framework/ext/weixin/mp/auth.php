<?php
namespace weixin\mp;

require_once "helper.php";
require_once "access_token.php";

class auth {

	private $access_token;
	private $app_id;
	private $app_secret;

	public function __construct() {
		$this -> access_token = new access_token();

		$app_config = load_config();
		$this -> app_id = $app_config -> app_id;
		$this -> app_secret = $app_config -> app_secret;
	}

	/**
	 * 在请求的企业微信接口后面自动附加token信息
	 */
	private function append_token($url) {
		$token = $this -> access_token -> get_access_token();

		if (strrpos($url, "?", 0) > -1) {
			return $url . "&access_token=" . $token;
		} else {
			return $url . "?access_token=" . $token;
		}
	}

	public function get_redirect_url($redirect_url,$state) {
		$redirect_url = urlencode($redirect_url);
		$state=encrypt($state);
		$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->app_id}&redirect_uri={$redirect_url}&response_type=code&scope=snsapi_base&state={$state}#wechat_redirect";
		return $url;
	}

	public function get_base_info($code) {
		$url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->app_id}&secret={$this -> app_secret}&code={$code}&grant_type=authorization_code";
		$ret = http_get($url);
		return $ret["content"];
	}

}
