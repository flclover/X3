<?php

class socket_client {
    var $client_secret_key = 'aaaa';
    var $server = '127.0.0.1';
    var $port = '8282';
    var $header = '';
    var $client = null;

    function __construct() {
        $this -> header = "Client-Sec-Key: " . (md5(md5($this -> client_secret_key))) . "\r\n";
        $this -> client = @stream_socket_client('tcp://' . $this -> server . ':' . $this -> port, $errno, $errstr, 30);
    }

    function send($msg) {
        $body = json_encode($msg) . "\r\n";
        $res = @stream_socket_sendto($this -> client, $this -> header . $body);
    }
}
