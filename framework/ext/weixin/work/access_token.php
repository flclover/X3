<?php

require_once "helper.php";

class access_token {
	private $corp_id;
	private $agent_id;
	private $secret;

	public function __construct($agent_id) {

		$this -> corp_id = get_system_config("weixin_corp_id");
		$this -> agent_id = $agent_id;

		if ($agentId == "txl") {
			$this -> secret = get_system_config("weixin_txl_secret");
		}
		if ($agentId == "helper") {
			$this -> secret = get_system_config("weixin_helper_secret");
		}
	}

	public function get_access_token() {

		$path = TEMP_PATH . $this -> agent_id . '.php';
		$data = json_decode(get_php_file($path));
		if ($data -> expire_time < time()) {

			$url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={$this->corp_id}&corpsecret={$this->secret}";
			$res = http_get($url);
			$res = json_decode($res);

			$access_token = $res -> access_token;

			if ($access_token) {
				$data -> expire_time = time() + 7000;
				$data -> access_token = $access_token;
				set_php_file($path, json_encode($data));
			}
		} else {
			$access_token = $data -> access_token;
		}
		return $access_token;
	}

}
