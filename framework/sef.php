<?php
/*--------------------------------------------------------------------
 小微OA系统 - 移动信息化定制专家

 Copyright (c) 2013-2017 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xaf
 --------------------------------------------------------------*/
//'Asia/Shanghai'   亚洲/上海
date_default_timezone_set('Asia/Shanghai');

// 版本信息
define('XAF_VERSION', 0.1);

define('CORE_PATH', FRAMEWORK_PATH . 'core' . DS);
define('CONF_PATH', FRAMEWORK_PATH . 'conf' . DS);
define('EXT_PATH', FRAMEWORK_PATH . 'ext' . DS);

defined('ROOT_PATH') or define('ROOT_PATH', dirname(realpath(APP_PATH)) . DS);
defined('RUNTIME_PATH') or define('RUNTIME_PATH', ROOT_PATH . 'runtime' . DS);
defined('CACHE_PATH') or define('CACHE_PATH', RUNTIME_PATH . 'cache' . DS);
defined('TEMP_PATH') or define('TEMP_PATH', RUNTIME_PATH . 'temp' . DS);
defined('LOG_PATH') or define('LOG_PATH', RUNTIME_PATH . 'log' . DS);

define('IS_CLI', PHP_SAPI == 'cli' ? true : false);
define('IS_WIN', strpos(PHP_OS, 'WIN') !== false);
define('IS_GET', $_SERVER['REQUEST_METHOD'] == 'GET' ? true : false);
define('IS_POST', $_SERVER['REQUEST_METHOD'] == 'POST' ? true : false);

if (!empty($_REQUEST['is_ajax'])) {
	define('IS_AJAX', true);
} elseif (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	define('IS_AJAX', true);
} else {
	define('IS_AJAX', false);
}
// 加载核心
require CORE_PATH . 'core.php';

// 应用初始化
sef\core::start();
?>
