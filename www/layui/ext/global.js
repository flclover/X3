layui.define(['layer', 'laydate', 'toastr'], function(exports) {
	window.$ = layui.jquery;
	var layer = layui.layer;
	var laydate = layui.laydate;

	top_menu_id = get_cookie("top_menu_id");
	$(".top-menu a[node=" + top_menu_id + "]").addClass("active");

	$('.top-menu a').on('click', function() {
		$this = $(this);
		url = $this.attr("url");
		node = $this.attr("node");
		set_cookie("top_menu_id", node);
		set_cookie("left_menu", "");
		set_cookie("current_node", "");
		("http://" === url.substr(0, 7) || "#" === url.substr(0, 1)) ? window.open(url) : location.href = url;
	});

	$('.sidebar a').on('click', function() {
		$this = $(this);
		url = $this.attr("url");
		node = $this.attr("node");
		set_cookie("current_node", node);
		if (url.substr(0, 1) == '#') {
			return false;
		}
		("http://" === url.substr(0, 7) || "#" === url.substr(0, 1)) ? window.open(url) : location.href = url;
	});

	$('.tab-header > a').on('click', function() {
		$this = $(this);
		$parent = $this.parent();
		$parent.find('a').removeClass('active');
		$this.addClass('active');
		idx = $parent.find('a').index($this);

		$parent.parent().find('.tab-content > div').removeClass('active');
		$parent.parent().find('.tab-content > div').eq(idx).addClass('active');
	});

	current_node = get_cookie("current_node");
	$(".sidebar a[node='" + current_node + "']").addClass("active");
	$(".sidebar a.active").parents('li').find('input').attr('checked', 'checked');

	$(".x-sidebar .nav a[node='" + current_node + "']").closest("li").addClass("current");
	$(".x-sidebar .nav a[node='" + current_node + "']").parents("li").each(function() {
		$(this).addClass("active open");
	});

	$('#toggle-top-menu').on('click', function() {
		if ($('.main').hasClass('m-hidden')) {
			$('.main').removeClass('m-hidden');
		} else {
			$('.main').addClass('m-hidden');
		}
	});

	$('#submit_search').on('click', function() {
		$('#form_search').submit();
	});

	$('#form_search #keyword').on('keydown', function(event) {
		if (event.keyCode == 13) {
			$('#form_search').submit();
		}
	});

	$('#toggle_adv_search').on('click', function() {
		toggle_adv_search();
	});

	$('#close_adv_search').on('click', function() {
		toggle_adv_search();
	});

	function toggle_adv_search() {
		if ($("#adv_search").attr("class").indexOf("hidden") < 0) {
			$("#adv_search").addClass("hidden");
			$("#toggle_adv_search_icon").addClass("fa-angle-down");
			$("#toggle_adv_search_icon").removeClass("fa-angle-up");
		} else {
			$("#adv_search").removeClass("hidden");
			$("#toggle_adv_search_icon").addClass("fa-angle-up");
			$("#toggle_adv_search_icon").removeClass("fa-angle-down");
		}
	};

	$('#submit_adv_search').on('click', function() {
		$('#form_adv_search').submit();
	});

	var start = {
		choose : function(datas) {
			end.min = datas;
			//开始日选好后，重置结束日的最小日期
			end.start = datas;
			//将结束日的初始值设定为开始日
		}
	};

	$('.input-start-date').on('click', function() {
		start.elem = this;
		start.show = true;
		laydate(start);
	});

	var end = {
		choose : function(datas) {
			start.max = datas;
			//结束日选好后，重置开始日的最大日期
		}
	};

	$('.input-end-date').on('click', function() {
		end.elem = this;
		laydate(end);
	});

	$('.input-date').on('click', function() {
		laydate.render({
			elem : this,
			show : true,
			theme : '#2794eb'
		});
	});

	$('.input-date-time').on('click', function() {
		laydate.render({
			elem : this,
			type : 'datetime',
			show : true,
			format : 'yyyy-MM-dd HH:mm',
			theme : 'xui'
		});
	});

	$('.toggle-select-all').on('click', function() {
		$this = $(this);
		var count = 0;
		var checkbox_name = $this.attr('data');
		is_checked = $this.attr('checked');
		if (is_checked) {
			$("input[type=checkbox][name='" + checkbox_name + "']").each(function() {
				this.checked = false;
				$(this).closest('.tbody').removeClass('selected');
			});
			$this.attr('checked', false);
		} else {
			$("input[type=checkbox][name='" + checkbox_name + "']").each(function() {
				this.checked = true;
				$(this).closest('.tbody').addClass('selected');
			});
			$this.attr('checked', true);
		}
	});

	$(".dropdown-toggle").on('click', function() {
		$next = $(this).next().toggle();
	});

	$(".dropdown-menu").on('mouseleave', function() {
		$(this).toggle();
	});

	$(document).on('click', '.multi-select-box .data-list i.del', function() {
		$(this).parent().remove();
	});

	var exp = {
		winopen : function(url, w, h) {
			url = fix_url(url);
			var index = layer.open({
				type : 2,
				title : false,
				area : [w + 'px', h + 'px'],
				shade : 0.6,
				closeBtn : 0,
				shadeClose : false,
				scrollbar : false,
				content : [url, 'no']
			});
			if (is_mobile()) {
				layer.full(index);
			}
		},
		send_form : function(from_id, post_url, return_url, callback) {
			check_form(from_id, function(ret) {
				if (ret.status) {
					if (return_url) {
						set_return_url(return_url);
					}
					if ($("#ajax").val() == 1) {
						var vars = $("#" + from_id).serialize();
						$.ajax({
							type : "POST",
							url : post_url,
							data : vars,
							dataType : "json",
							success : function(data) {
								callback(data);
							}
						});
					} else {
						//取消beforeunload事件
						$(window).unbind('beforeunload', null);
						$("#" + from_id).attr("action", post_url);
						$("#" + from_id).submit();
					}
				} else {
					layer.msg(ret.info);
					ret.dom.focus();
					return false;
				}
			});
		},
		/* ajax提交*/
		send_ajax : function(url, vars, callback) {
			return $.ajax({
				type : "POST",
				url : url,
				data : vars,
				dataType : "json",
				success : callback
			});
		},
	};
	win_exp(exp);
	exports('global', {});
});
