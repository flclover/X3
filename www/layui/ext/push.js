layui.define(['toastr'], function(exports) {
	var $ = layui.jquery;
	function get_push() {
		$.getJSON(push_server, function(ret) {
			if (ret.status) {
				$content = eval('(' + ret.data.data + ')');
				push_info($content);
			}
			if (ret.status > 0) {
				get_push();
			}
		});
	}

	function push_info($msg) {
		var position;
		if ($msg.action.length) {
			$title = '<h3>[' + $msg.type + '] [' + $msg.action + ']</h3>';
		} else {
			$title = '<h3>[' + $msg.type + ']</h3>';
		}
		$content = '<p class="push-title">' + $msg.title + '</p>' + $msg.content;

		if (is_mobile()) {
			position = "toast-top-full-width";
		} else {
			position = "toast-bottom-right";
		}
		toastr.options = {
			"closeButton" : true,
			"positionClass" : position,
			"timeOut" : ws_push_time * 1000000
		};
		toastr.info($content, $title);
	}

	get_push();

	// 连接服务端
	var socket = io('http://' + document.domain + ':2120');
	// 连接后登录
	socket.on('connect', function() {
		socket.emit('login', '{:get_user_id()}');
	});

	// 后端推送来消息时
	socket.on('new_msg', function(msg) {
		get_push();
	});

	window.push_info = push_info;
	exports('push', {});
});

